<?php

use Illuminate\Support\Facades\Route;

//  / => index
Route::get('/', function () {
    return view('index');
});

// /mensaje => mensaje
Route::get('/mensaje', function () {
    return view('mensaje', [
        'mensaje' => 'Hola clase'
    ]);
});

// /listado => listado
Route::get('/listado', function () {
    $datos = [
        "Ramon",
        "Eva",
        "Carmen",
    ];

    return view('listado', [
        'datos' => $datos
    ]);
});

// /imagen => imagen
Route::get('/imagen', function () {
    return view('imagen');
});
